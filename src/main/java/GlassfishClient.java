import interfaces.NoteBeanInterfaceR;
import interfaces.UserBeanInterfaceR;
import util.Util;

import javax.naming.Context;
import javax.naming.InitialContext;
import java.util.Properties;

public class GlassfishClient {
    static final String USERBEAN_JNDI = "java:global/ejb/UserBean!interfaces.UserBeanInterfaceR";
    static final String NOTEBEAN_JNDI = "java:global/ejb/NoteBean!interfaces.NoteBeanInterfaceR";

    public static void main(String[] args) throws Exception{
        Properties props = new Properties();
        props.put(Context.INITIAL_CONTEXT_FACTORY, "com.sun.enterprise.naming.SerialInitContextFactory");
        props.setProperty("org.omg.CORBA.ORBInitialHost", "localhost");
        props.setProperty("org.omg.CORBA.ORBInitialPort", "3700");
        Context context = new InitialContext(props);

        UserBeanInterfaceR userBean = (UserBeanInterfaceR) context.lookup(USERBEAN_JNDI);
        NoteBeanInterfaceR noteBean = (NoteBeanInterfaceR) context.lookup(NOTEBEAN_JNDI);

        Util.createUsers(userBean);
        Util.createNotes(noteBean,userBean);
    }
}
