import interfaces.NoteBeanInterfaceR;
import interfaces.UserBeanInterfaceR;
import util.Util;

import javax.naming.Context;
import javax.naming.InitialContext;
import java.util.Properties;


public class WildflyClient {
    static final String USERBEAN_JNDI = "ejb/UserBean!interfaces.UserBeanInterfaceR";
    static final String NOTEBEAN_JNDI = "ejb/NoteBean!interfaces.NoteBeanInterfaceR";

    public static void main(String[] args) throws Exception {
        Properties jndiProps = new Properties();
        jndiProps.put(Context.INITIAL_CONTEXT_FACTORY, "org.wildfly.naming.client.WildFlyInitialContextFactory");
        jndiProps.put(Context.PROVIDER_URL, "remote+http://localhost:8080");
        jndiProps.put(Context.SECURITY_PRINCIPAL, "b");
        jndiProps.put(Context.SECURITY_CREDENTIALS, "a");
        Context context = new InitialContext(jndiProps);

        UserBeanInterfaceR userBean = (UserBeanInterfaceR) context.lookup(USERBEAN_JNDI);
        NoteBeanInterfaceR noteBean = (NoteBeanInterfaceR) context.lookup(NOTEBEAN_JNDI);
        Util.createUsers(userBean);
        Util.createNotes(noteBean,userBean);
    }
}
