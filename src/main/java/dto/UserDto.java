package dto;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;

public class UserDto implements Serializable {

    String user_id;
    String username;
    String password;
    Collection<NoteDto> notes = new ArrayList<>();

    public UserDto() {
    }

    public UserDto(String username, String password) {
        this.username = username;
        this.password = password;
    }

    public UserDto(String username, String password, String user_id) {
        this(username, password);
        this.user_id = user_id;
    }


    public String getUser_id() {
        return user_id;
    }

    public void setUser_id(String user_id) {
        this.user_id = user_id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public Collection<NoteDto> getNotes() {
        return notes;
    }

    public void setNotes(Collection<NoteDto> notes) {
        this.notes = notes;
    }

    @Override
    public String toString() {
        return "UserDto{" +
                "user_id='" + user_id + '\'' +
                ", username='" + username + '\'' +
                ", password='" + password + '\'' +
                '}';
    }

}
