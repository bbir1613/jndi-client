package interfaces;

import dto.UserDto;

public interface UserBeanInterfaceR {

    UserDto create(UserDto userDto);

    UserDto findUserDto(String username);
}
