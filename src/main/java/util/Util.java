package util;

import dto.NoteDto;
import dto.UserDto;
import interfaces.NoteBeanInterfaceR;
import interfaces.UserBeanInterfaceR;

public class Util {

    public static void createUsers(UserBeanInterfaceR userBean) {
        String TAG = "createUsers";
        String username = "jndi_user";
        String password = "a";
        Log(TAG, "begin");
        UserDto userDto = new UserDto(username, password);
        for (int i = 0; i < 10; ++i) {
            Log(TAG, userBean.create(userDto));
            userDto.setUsername(username + i);
        }
        Log(TAG, "ended");
    }

    public static void createNotes(NoteBeanInterfaceR noteBean, UserBeanInterfaceR userBean) {
        String TAG = "createNotes";
        Log(TAG, "begin");
        UserDto userDto = userBean.findUserDto("jndi_user");
        NoteDto noteDto = new NoteDto("jndi_note");
        noteDto.setUserDto(userDto);
        Log(TAG,noteBean.create(noteDto));

        noteDto = new NoteDto("jndi_note2");
        noteDto.setUserDto(userDto);
        Log(TAG,noteBean.create(noteDto));
        Log(TAG, "ended");
    }

    public static void Log(String tag, Object... objs) {
        if (objs != null) {
            StringBuilder sb = new StringBuilder();
            sb.append(tag).append(" : ");
            for (Object obj : objs) {
                sb.append(obj.toString()).append(" \t");
            }
            System.out.println(sb.toString());
        }
    }
}
